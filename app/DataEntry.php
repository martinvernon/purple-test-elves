<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataEntry extends Model
{
    protected $fillable = ['created_at', 'entry'];
}
