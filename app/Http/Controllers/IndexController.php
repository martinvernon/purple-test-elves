<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataEntry;

/**
 * Class IndexController
 * 
 * Responsible for running the logic to pull data from the database, sort it as required and return the results as required.
 */
class IndexController extends Controller
{
    /**
     * Variable $the_guard
     * 
     * used to store a guard id where required
     *
     * @var integer
     */
    protected $the_guard = 0;

    /**
     * Variable $last_guard
     * 
     * used in loops to send the previously detected guard dependant on the logic
     *
     * @var integer
     */
    protected $last_guard = 0;

    /**
     * Variable $guards
     * 
     * main array used once data has been pulled from the database and sorted in the constructor
     *
     * @var array
     */
    protected $guards = [];

    /**
     * Variable $start
     *
     * used to store various start dates when logic requires it, used in the main loop
     * @var \Carbon
     */
    protected $start;

    /**
     * Variable $end
     *
     * used to store various start dates when logic requires it, used in the main loop
     * @var \Carbon
     */
    protected $end;
    
    /**
     * Function __construct
     * 
     * Used to pull the data from the database, then filter the returned collection.
     * Logic performed is to grab the start datetime and the end datetime for a guards sleep entry, 
     * calculate the duration between the two and update a total duration for the guard in question.
     * The resulting array is then used in /step1 and /step2 to return the required results and pass the variables to the view
     *
     * @param Request $request
     */
    function __construct(Request $request){
        
        $this->middleware(function($request, $next) {
            // get imput data - returned as a collection
            $data = DataEntry::orderby('created_at')->get();

            // filter / loop through the collection
            $data->filter(function($entry){

                // detect if the entry contains a # - this indicates that it is a guard id entry
                if(strpos($entry->entry, '#') !== false){

                    // extract the guard id as integer
                    $guard_id = $this->extract_guard_id($entry);
                    
                    // if there is a guard id create an entry for it in the main guard array
                    if($guard_id[0] != $this->last_guard){

                        // log last detected guard id
                        $this->last_guard = $guard_id[0];

                        // check to see if an entry for the guard id already exists, if not create one
                        if(!in_array($this->last_guard, array_keys($this->guards))){
                            $this->guards[$this->last_guard] = [
                                'total_duration' => 0,
                                'entries' => []
                            ];
                        }
                    }
                
                // if the entry does not contain a # then it is a starting time entry or an ending time entry    
                }else{
                    
                    // if the entry contains the string falls asleep this indicates a starting time for a guards sleep
                    if(strpos($entry->entry, 'falls asleep') !== false){
                        
                        // store the detected timestamp for later retrevial in the loop
                        $this->start = $entry->created_at;
                    }   
                    
                    // if the entry contains the string wakes up this indicates an ending time for a guards sleep
                    if(strpos($entry->entry, 'wakes up') !== false){
                        
                        // store the detected timestamp for later retrevial in the loop
                        $end = $entry->created_at;
                        
                        // using the stored start and end timestamos, store a new complete date entry for a guard sleeping
                        $this->guards[$this->last_guard]['entries'][] = [
                            'start_min' => $this->start->format('i'),
                            'end_min' => $end->format('i'),
                        ];

                        // using Carbon calculate the duration of the sleep - for more info on Carbon see here : https://carbon.nesbot.com/docs/
                        $this->guards[$this->last_guard]['total_duration'] += $this->start->diffInMinutes($end);
                    }
                }
            });

            return $next($request);
        });
    }

    /**
     * Function step1
     * 
     * used to calculate data for the answer to step1 and send to the view for user consuption
     *
     * @param Request $request
     * @return void
     */
    function step1(Request $request){
        
        // detect the guard that sleeps the most
        $the_guard = $this->get_the_guard_id();

        // detect the counts amount of times the guard has been asleep for each min
        $mins = $this->guard_mins($the_guard);
        
        // sort the returned mins array and detect the largest value - this is the min that the guard has been asleep the most
        $max_min = array_keys($mins, max($mins));

        // return the data to the view for display
        return view('index', [
            'guard_id' => $the_guard,
            'max_min' => $max_min[0],
            'answer' => $the_guard * $max_min[0],
            'route' => route('step2'), // used for the href attribute in the link to provide very basic navigation to /step2
            'title' => 'Step 2' // used for the anchor text in the link to provide very basic navigation to /step2
        ]);
    }

    /**
     * Function step2
     * 
     * used to calculate data for the answer for step2 and send to the view for user consuption
     *
     * @param Request $request
     * @return void
     */
    function step2(Request $request){
        $the_guard = 0;
        $max_count = 0;

        // using the main guard array, loop through each and get their calculated min counts
        foreach($this->guards as $guard_id => $guard){
            $this->guards[$guard_id]['mins'] = $this->guard_mins($guard_id);
        }
        
        // loop through each available min
        for($i = 1; $i <= 59; $i++){

            // detect the guard that is asleep the most amount of times for the chosen min
            $mins[$i] = $this->get_top_guard($i);

            // if the detected min count for the chosen guard is higher than the previous detected min count,
            // then overwrite the largest min count at that time
            if($mins[$i]['count'] > $max_count){
                $max_count = $mins[$i]['count']; // the detected count of mins
                $the_guard = $mins[$i]['guard']; // the detected guard that has been asleep the most for that min
                $max_min = $i; 
            }
        }

        // return the data to the view for display
        return view('index', [
            'guard_id' => $the_guard,
            'max_min' => $max_min,
            'answer' => $the_guard * $max_min,
            'route' => route('step1'), // used for the href attribute in the link to provide very basic navigation back to /step1
            'title' => 'Step 1' // used for the anchor text in the link to provide very basic navigation back to /step1
        ]);
    }

    /**
     * Function extract_guard_id
     * 
     * use a regular expression to extract the integer of the guard id from the entry string
     * uses the # character - presumption made is that all digits after a # is a guard id
     *
     * @param string $entry
     * @return void
     */
    function extract_guard_id($entry){
        $regex = '/#(\d*)/m';
        preg_match_all($regex, $entry, $matches);
        if(is_array($matches) && isset($matches[1]))
            return $matches[1];
        
        return;
    }

    /**
     * Function get_the_guard_id
     * 
     * uses the main data array to detect the guard that has been asleep the most
     *
     * @return void
     */
    function get_the_guard_id(){
        $longest_asleep = 0;

        // loop through the main data array
        foreach($this->guards as $guard_id => $guard_info){

            // if the looped duration is greater than the last detected number, overwrite data
            if($guard_info['total_duration'] > $longest_asleep){
                $the_guard = $guard_id;
                $longest_asleep = $guard_info['total_duration'];
            }            
        }

        // return the detected guard id for the guard with the higest duration of sleep
        return $the_guard;
    }

    /**
     * Function guard_mins
     *
     * uses the main data array to count the number of times a chosen guard has been asleep for a particular min
     * 
     * @param int $guard_id
     * @return void
     */
    function guard_mins($guard_id){
        $mins = [];

        // loop through the main data array
        foreach($this->guards[$guard_id]['entries'] as $entry){

            // using the starting and ending mins, loop through each detected min
            for($i = (int)$entry['start_min']; $i < (int)$entry['end_min']; $i++){
                
                // if an entry for the chosen min exists then increment
                if(!empty($mins[$i])){
                    $mins[$i]++;
                
                // if an entry for the chosen min doesn't exist then initiate as 1
                }else{
                    $mins[$i] = 1;
                }
            }
        }

        // return the guards mins array
        return $mins;
    }

    /**
     * Function get_top_guard
     *
     * uses chosen min to loop through main guard array to detect the guard id that has been asleep the most for that chosen min
     * 
     * @param int $min
     * @return void
     */
    function get_top_guard($min){
        $the_guard = 0;
        $max_count = 0;
        
        // loop through the main data array
        foreach($this->guards as $guard_id => $guard){

            // if the min entry for that guard exists and is greated than the detected max count, then overwrite with guard data
            if(!empty($guard['mins'][$min]) && $guard['mins'][$min] > $max_count){
                $the_guard = $guard_id;
                $max_count = $guard['mins'][$min];
            }
        }

        // return detected guard data
        return [
            'guard' => $the_guard,
            'count' => $max_count
        ];
    }
}
