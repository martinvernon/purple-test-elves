<?php

namespace App\Imports;

use App\DataEntry;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;

class DataImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new DataEntry([
            'created_at' => Carbon::createFromFormat('Y-m-d H:i', $row[0]),
            'entry' => $row[1]
        ]);
    }
}
