<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\DataImport;
use Excel;

class ImportDataEntries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from storage/input-data/input-data.xlsx';

    /**
     * The filename of the import file.
     *
     * @var string
     */
    protected $import_file = '';

    /**
     * Create a new command instance.
     *
     * sets the import file location
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->import_file = storage_path('/input-data/input-data.xlsx');
    }

    /**
     * Execute the console command.
     * 
     * uses the maatwebsite/excel package to import excel data into database
     * for more information on maatwebsite/excel see here - https://docs.laravel-excel.com/3.1/getting-started/
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting loading data');
        Excel::import(new DataImport, $this->import_file);
        $this->info('Finished loading data');
    }
}
