## Martin Vernon - Tech Test

Based from https://adventofcode.com/2018/day/4 and https://adventofcode.com/2018/day/4#part2

## Install instructions</p>

This tech test uses on Laravel 5.8 and presumes that you have [composer](https://getcomposer.org/) installed and a working knowledge of Laravel projects using MySQL.

- Clone the git repo
- Run composer install
- Configure your .env file to point at an accessible MySQL server
- Run php artisan migrate to populate the database tables needed to run this code
- Run php artisan import:data to import the input data as provided by Advent of Code which has been downloaded an put into /storage/input-data/input-data.xlsx
- Once data is imported navigate to /step1 to see the results
- Then navigate to /step2 to see the second stage results

## Unit tests
Apologies, at the time of writing this application, my Macbook wouldn't run phpunit so I was not able to write and run any tests to cover my functions.