<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect()->route('step1');
});
Route::get('/step1', ['as'=>'step1', 'uses'=>'IndexController@step1']);
Route::get('/step2', ['as'=>'step2', 'uses'=>'IndexController@step2']);
